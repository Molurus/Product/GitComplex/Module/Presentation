/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Tree/View/Detail/TreeObserver.h>
#include <GitComplex/Repository/Tree/Presenter/Column.h>

#include <QHeaderView>
#include <QTimer>

/*!
 * \file src/GitComplex/Repository/Tree/View/Detail/TreeObserver.cpp
 * \brief src/GitComplex/Repository/Tree/View/Detail/TreeObserver.cpp
 */

namespace GitComplex::Repository::Tree::View::Detail
{

Observer::Observer(QAbstractItemModel* model, QWidget* parent)
    : QTreeView{parent}
{
    setContextMenuPolicy(::Qt::CustomContextMenu);

    setModel(model);

    adjustInitialView();

    m_sections_size_timer.setSingleShot(true);
    connect(&m_sections_size_timer, &QTimer::timeout,
            this, &Observer::adjustSectionsSize);
}

void Observer::reset()
{
    QTreeView::reset();

    using Presenter::Column;

    hideColumn(Column::Index::IsChecked);

    expandAll();

    delayAdjustSectionsSize();

    QMetaObject::invokeMethod(this, &Observer::adjustInitialSelection,
                              Qt::QueuedConnection);
}

void Observer::dataChanged(const QModelIndex&  top_left,
                           const QModelIndex&  bottom_right,
                           const QVector<int>& roles)
{
    QTreeView::dataChanged(top_left, bottom_right, roles);

    using Presenter::Column;

    if (  top_left.column() <= Column::Index::Branch
       && Column::Index::Branch <= bottom_right.column())
        delayAdjustSectionsSize();
}

bool Observer::hasRepositories() const
{
    QAbstractItemModel* model = this->model();
    return model && model->rowCount() > 0;
}

void Observer::adjustInitialView()
{
    setAlternatingRowColors(true);
    setSelectionMode(QAbstractItemView::ExtendedSelection);
    setTextElideMode(Qt::ElideLeft);
    header()->setMinimumSectionSize(20);
    header()->setStretchLastSection(false);
}

void Observer::adjustInitialSelection()
{
    using Presenter::Column;

    if (hasRepositories())
        setCurrentIndex(model()->index(0, Column::Index::Path));
}

void Observer::adjustSectionsSize()
{
    using Presenter::Column;

    QHeaderView* header = this->header();

    header->setSectionResizeMode(QHeaderView::ResizeToContents);

    int branch_section_size = header->sectionSize(Column::Index::Branch);

    header->setSectionResizeMode(Column::Index::Path,   QHeaderView::Stretch);
    header->setSectionResizeMode(Column::Index::Branch, QHeaderView::Interactive);

    header->resizeSection(Column::Index::Branch, branch_section_size);
}

void Observer::delayAdjustSectionsSize()
{
    m_sections_size_timer.start(250);
}

} // namespace GitComplex::Repository::Tree::View::Detail
