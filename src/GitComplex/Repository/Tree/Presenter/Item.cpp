/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Tree/Presenter/Item.h>

#include <QDir>

#include <algorithm>

/*!
 * \file src/GitComplex/Repository/Tree/Presenter/Item.cpp
 * \brief src/GitComplex/Repository/Tree/Presenter/Item.cpp
 */

namespace GitComplex::Repository::Tree::Presenter
{

IdList Item::List::merge(Item::List other)
{
    IdList ids;

    for (auto& item : other)
    {
        auto it = std::find_if(begin(), end(), [&](const auto& own)
                               { return (*own).id() == (*item).id(); });

        // NOTE: Perhaps it may need to be divided into "add" and "update".
        ids.push_back((*item).id());

        if (it != end())
            *it = std::move(item);
        else
            push_back(std::move(item));
    }

    return ids;
}

IdList Item::List::remove(const IdList& ids)
{
    IdList removed_ids;

    for (auto& id : ids)
    {
        auto it = std::find_if(begin(), end(), [&](const auto& own)
                               { return (*own).id() == id; });

        if (it != end())
        {
            erase(it);
            removed_ids.push_back(id);
        }
    }

    return removed_ids;
}

Item::WeakOptional Item::List::findItem(const SingleId& id) const
{
    if (empty())
        return {};

    for (const auto& item : *this)
    {
        if ((*item).id() == id)
            return item;

        if (const auto& sub_item = (*item).submodules().findItem(id))
            return sub_item;
    }

    return {};
}

std::size_t Item::index() const
{
    if (m_parent)
    {
        auto it = std::find_if(m_submodules.begin(),
                               m_submodules.end(),
                               [this](const auto& u_ptr)
                               { return u_ptr == this; });
        return std::distance(m_submodules.begin(), it);
    }

    return 0;
}

Path Item::extractSubmodulePath() const
{
    if (auto loc = otn::gain(parent()))
    {
        const auto& parent = *loc;
        assert(m_superproject_id && m_superproject_id == parent.id());
        return QDir{parent.path()}.relativeFilePath(path());
    }

    return {};
}

} // namespace GitComplex::Repository::Tree::Presenter
