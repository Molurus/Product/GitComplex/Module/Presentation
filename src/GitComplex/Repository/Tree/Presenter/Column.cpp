/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Tree/Presenter/Column.h>

/*!
 * \file src/GitComplex/Repository/Tree/Presenter/Column.cpp
 * \brief src/GitComplex/Repository/Tree/Presenter/Column.cpp
 */

namespace GitComplex::Repository::Tree::Presenter
{

const std::array<QString, Column::Count>
Column::DisplayName{
    QString{""},  // Path
    QString{""},  // Branch
    QString{"M"}, // Modification
    QString{"L"}, // Log
    QString{"T"}, // Terminal
    QString{}     // IsChecked
};

const std::array<QString, Column::Count>
Column::HeaderName{
    QString{"Path"},   // Path
    QString{"Branch"}, // Branch
    QString{"M"},      // Modification
    QString{"L"},      // Log
    QString{"T"},      // Terminal
    QString{"C"}       // IsChecked
};

const std::array<QString, Column::Count>
Column::ToolTip{
    QString{},                     // Path
    QString{},                     // Branch
    QString{"Show modifications"}, // Modification
    QString{"Show Log"},           // Log
    QString{"Open terminal"},      // Terminal
    QString{}                      // IsChecked
};

} // namespace GitComplex::Repository::Tree::Presenter
