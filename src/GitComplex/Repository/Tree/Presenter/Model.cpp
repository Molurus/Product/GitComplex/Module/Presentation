/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Tree/Presenter/Model.h>
#include <GitComplex/Repository/Tree/Presenter/Column.h>
#include <GitComplex/Repository/Tree/Presenter/DataRole.h>

#include <QBrush>

#include <unordered_set>

/*!
 * \file src/GitComplex/Repository/Tree/Presenter/Model.cpp
 * \brief src/GitComplex/Repository/Tree/Presenter/Model.cpp
 */

namespace GitComplex::Repository::Tree::Presenter
{

namespace
{

Item::WeakSingleConst internalItemFrom(const QModelIndex& index)
{ return Item::WeakSingleConst{static_cast<const Item*>(index.internalPointer())}; }

Item::WeakSingle internalMutableItemFrom(const QModelIndex& index)
{ return Item::WeakSingle{static_cast<Item*>(index.internalPointer())}; }

} // namespace

Model::Model(QObject* parent)
    : QAbstractItemModel{parent},
      m_items{},
      m_structure_observable_delegate{otn::itself}
{}

int Model::rowCount(const QModelIndex& parent) const
{
    if (!hasItems() || parent.column() > 0)
        return 0;

    if (!parent.isValid())
        return m_items.size();

    return (*internalItemFrom(parent)).submodules().size();
}

int Model::columnCount(const QModelIndex&) const
{
    return Column::Count;
}

QModelIndex Model::index(int row, int column, const QModelIndex& parent) const
{
    if (!hasItems() || !hasIndex(row, column, parent))
        return {};

    Item::WeakOptional child_item;

    if (!parent.isValid())
        child_item = m_items.item(row);
    else
        child_item = (*internalItemFrom(parent)).submodule(row);

    if (child_item)
        return createIndex(row, column, static_cast<void*>(child_item));

    return {};
}

QModelIndex Model::parent(const QModelIndex& index) const
{
    if (!hasItems() || !index.isValid())
        return {};

    if (const auto& parent_item = (*internalItemFrom(index)).parent())
        return createIndex((*parent_item).index(), 0,
                           static_cast<void*>(parent_item));

    return {};
}

::Qt::ItemFlags Model::flags(const QModelIndex& index) const
{
    ::Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    if (!index.isValid())
        return flags;

    if (index.column() == Column::Index::IsChecked)
        flags.setFlag(::Qt::ItemIsUserCheckable);

    if (!(  index.column() == Column::Index::Path
         || index.column() == Column::Index::Branch))
        flags.setFlag(::Qt::ItemIsSelectable, false);

    return flags;
}

QVariant Model::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return {};

    const auto& internal_item = internalItemFrom(index);
    const Item& item = *internal_item;

    switch (role)
    {
    case ::Qt::DisplayRole:
    {
        switch (index.column())
        {
        case Column::Index::Path:
        {
            if (item.isSubmodule())
                return QVariant{item.submodulePath()};

            return QVariant{item.path()};
        }
        case Column::Index::Branch:
            return QVariant{item.branch()};
        default:
            return QVariant{Column::DisplayName.at(index.column())};
        }
    }
    case ::Qt::ToolTipRole:
        switch (index.column())
        {
        case Column::Index::Path:
            return QVariant{item.path()};
        case Column::Index::Branch:
            return QVariant{item.branch()};
        default:
            return QVariant{Column::ToolTip.at(index.column())};
        }
    case ::Qt::CheckStateRole:
    {
        switch (index.column())
        {
        case Column::Index::IsChecked:
            return QVariant{::Qt::Checked};
        default:
            return QVariant{};
        }
    }
    case ::Qt::ForegroundRole:
    {
        switch (index.column())
        {
        case Column::Index::Path:
            switch (item.directoryState())
            {
            case DirectoryState::NotExist:
                return QBrush{::Qt::red};
            case DirectoryState::NotGit:
                return QBrush{::Qt::gray};
            case DirectoryState::WorkTree:
                return QBrush{::Qt::black};
            }
            break;
        case Column::Index::Branch:
            return QBrush{item.branch() == "HEAD" ? ::Qt::red : ::Qt::black};
        case Column::Index::Modification:
            return QBrush{item.hasModifications() ? ::Qt::red : ::Qt::black};
        default:
            return QVariant{};
        } // switch
        break;
    }
    case InternalPointerRole:
        return QVariant{quintptr(static_cast<const Item*>(internal_item))};
    case IdRole:
        return QVariant::fromValue(OptionalId{item.id()});
    default:
        return QVariant{};
    } // switch

    return {};
}

bool Model::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (!index.isValid())
        return false;

    Item& item = *internalMutableItemFrom(index);

    bool is_changed = false;

    switch (role)
    {
    case ::Qt::EditRole:
    {
        switch (index.column())
        {
        case Column::Index::Branch:
            item.setBranch(value.toString());
            is_changed = true;
            break;
        case Column::Index::Modification:
            item.setHasModifications(value.toBool());
            is_changed = true;
            break;
        default:
            break;
        }
    }
    break;
    default:
        break;
    } // switch

    if (is_changed)
        emit dataChanged(index, index);

    return is_changed;
}

QVariant Model::headerData(int section,
                           ::Qt::Orientation orientation,
                           int role) const
{
    if (orientation == ::Qt::Vertical)
        return {};

    if (role != ::Qt::DisplayRole)
        return {};

    using Column = Column;

    return {Column::HeaderName.at(section)};
}

void Model::resetRepositories(Structure::List)
{
    beginResetModel();

    m_items.clear();

    endResetModel();
}

namespace
{

otn::slim::unique_single<Item> convert(Structure&&        structure,
                                       Item::WeakOptional parent_item)
{
    using ItemToken = otn::slim::unique_single<Item>;
    ItemToken item(otn::itself,
                   std::move(structure.id),
                   std::move(structure.path),
                   std::move(structure.directoryState),
                   std::move(structure.superprojectId),
                   parent_item);

    for (auto& submodule : structure.submodules)
        (*item).addSubmodule(convert(std::move(submodule), item));

    return item;
}

Item::List convert(Structure::List&& structures)
{
    Item::List items;

    for (auto& structure : structures)
    {
        auto item = convert(std::move(structure), nullptr);
        items.push_back(std::move(item));
    }

    return items;
}

} // namespace

void Model::addRepositories(Structure::List structures)
{
    if (structures.empty())
        return;

    beginResetModel();

    IdList added_ids = m_items.merge(convert(std::move(structures)));

    endResetModel();

    repositoriesAdded().repositoriesAdded(std::move(added_ids));
}

void Model::removeRepositories(IdList ids)
{
    if (ids.empty())
        return;

    beginResetModel();

    IdList removed_ids = m_items.remove(ids);

    endResetModel();

    repositoriesRemoved().repositoriesRemoved(std::move(removed_ids));
}

void Model::updateRepositoriesStatus(StatusList statuses)
{
    for (const auto& status:statuses)
        updateRepositoryStatus(status);
}

void Model::updateRepositoryStatus(const Status& status)
{
    const auto& item = m_items.findItem(status.id);
    if (!item)
        return;

    const QModelIndexList indices =
        match(index(0, 0),
              InternalPointerRole,
              QVariant{quintptr(static_cast<const Item*>(item))},
              1,
              ::Qt::MatchExactly | ::Qt::MatchRecursive);
    if (indices.count() != 1)
        return;

    const auto& row_index = indices.at(0);
    const auto& row       = row_index.row();
    const auto& parent    = row_index.parent();
    // TODO: Optimize 'emit dataChanged()'.
    setData(index(row, Column::Index::Branch, parent),       status.branch);
    setData(index(row, Column::Index::Modification, parent), status.hasModifications);
}

IdList Model::dependentSuperprojects(const IdList& ids) const
{
    std::unordered_set<SingleId> dependents;

    // TODO: Check for deep m_items and optimize.
    for (const auto& id:ids)
        otn::access(m_items.findItem(id), [&](const auto& item)
            {
                Item::WeakOptionalConst parent = item.parent();
                while (parent)
                {
                    dependents.insert((*parent).id());
                    parent = (*parent).parent();
                }
                dependents.insert(id);
            });

    return IdList{dependents.cbegin(), dependents.cend()};
}

namespace
{

void extractDependentSubmodules(const Item& item, IdList& ids)
{
    ids.push_back(item.id());
    for (const auto& submodule: item.submodules())
        extractDependentSubmodules(*submodule, ids);
}

} // namespace

IdList Model::dependentSubmodules(const IdList& ids) const
{
    IdList dependents;

    // TODO: Check for deep m_items and optimize.
    for (const auto& id:ids)
        otn::access(m_items.findItem(id), [&](const auto& item)
                    { extractDependentSubmodules(item, dependents); });

    return dependents;
}

OptionalId Model::repositoryId(const QModelIndex& index) const
{
    if (!index.isValid())
        return {};

    return (*internalItemFrom(index)).id();
}

Path Model::repositoryPath(const QModelIndex& index) const
{
    if (!index.isValid())
        return {};

    return (*internalItemFrom(index)).path();
}

} // namespace GitComplex::Repository::Tree::Presenter
