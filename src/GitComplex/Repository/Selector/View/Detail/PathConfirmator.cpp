/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Selector/View/Detail/PathConfirmator.h>

#include <QMessageBox>

/*!
 * \file src/GitComplex/Repository/Selector/View/Detail/PathConfirmator.cpp
 * \brief src/GitComplex/Repository/Selector/View/Detail/PathConfirmator.cpp
 */

namespace GitComplex::Repository::Selector::View::Detail
{

namespace
{

PathList confirmClosePaths(PathList paths)
{
    if (QMessageBox::warning(nullptr,
                             QMessageBox::tr("Close Repositories"),
                             QMessageBox::tr("Repositories to close:\n")
                             + paths.join('\n'),
                             QMessageBox::Ok | QMessageBox::Cancel,
                             QMessageBox::Cancel)
        == QMessageBox::Ok)
        return paths;

    return {};
}

} // namespace

PathList PathConfirmator::confirm(PathList paths) const
{
    if (m_confirmation == Confirmation::Close)
        paths = confirmClosePaths(std::move(paths));

    return paths;
}

} // namespace GitComplex::Repository::Selector::View::Detail
