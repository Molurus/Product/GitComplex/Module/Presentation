/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Selector/View/Detail/FileSystemSelector.h>

#include <QFileDialog>

/*!
 * \file src/GitComplex/Repository/Selector/View/Detail/FileSystemSelector.cpp
 * \brief src/GitComplex/Repository/Selector/View/Detail/FileSystemSelector.cpp
 */

namespace GitComplex::Repository::Selector::View::Detail
{

FileSystemSelector::FileSystemSelector()
    : m_event_delegate{otn::itself}
{}

void FileSystemSelector::save(Settings::RepositoryStorage& storage) const
{
    storage.setValue("LastDirectory", m_last_directory);
}

void FileSystemSelector::load(Settings::RepositoryStorage& storage)
{
    m_last_directory = storage.value("LastDirectory").toString();
}

PathList FileSystemSelector::repositories() const
{
    QString path = QFileDialog::
          getExistingDirectory(nullptr,
                               QFileDialog::tr("Select Repository Directory"),
                               m_last_directory,
                               QFileDialog::ShowDirsOnly
                               | QFileDialog::DontResolveSymlinks);

    if (!path.isEmpty())
    {
        QDir dir = path;
        dir.cdUp();
        m_last_directory = dir.path();
        return {path};
    }

    return {};
}

} // namespace GitComplex::Repository::Selector::View::Detail
