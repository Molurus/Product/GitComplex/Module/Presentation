/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Selector/View/Detail/TreeViewPathSelector.h>
#include <GitComplex/Repository/Selector/View/Detail/PathSelector.h>

/*!
 * \file src/GitComplex/Repository/Selector/View/Detail/TreeViewPathSelector.cpp
 * \brief src/GitComplex/Repository/Selector/View/Detail/TreeViewPathSelector.cpp
 */

namespace GitComplex::Repository::Selector::View::Detail
{

TreeViewPathSelector::TreeViewPathSelector(
    QPointer<const QAbstractItemView>      view,
    otn::weak_optional<const InfoProvider> info_provider,
    Confirmation confirmation)
    : m_view{std::move(view)},
      m_info_provider{std::move(info_provider)},
      m_confirmator{confirmation},
      m_notifier{*m_view}
{}

PathList TreeViewPathSelector::repositories() const
{
    if (auto selector = otn::try_make_object<PathSelector>(m_view, m_info_provider))
    {
        using Mode = PathSelector::Mode;
        PathList paths = (*selector).repositories(Mode::SelectedRoot);

        if (!paths.isEmpty())
            return m_confirmator.confirm(std::move(paths));
    }

    return {};
}

} // namespace GitComplex::Repository::Selector::View::Detail
