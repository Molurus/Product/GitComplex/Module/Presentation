/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Selector/View/Detail/PathSelector.h>

/*!
 * \file src/GitComplex/Repository/Selector/View/Detail/PathSelector.cpp
 * \brief src/GitComplex/Repository/Selector/View/Detail/PathSelector.cpp
 */

namespace GitComplex::Repository::Selector::View::Detail
{

namespace
{

void retrieveAllAny(const QAbstractItemModel&         model,
                    const QModelIndex&                index,
                    const PathSelector::InfoProvider& info_provider,
                    PathList& paths)
{
    int row_count = model.rowCount(index);
    for (int row = 0; row < row_count; ++row)
    {
        const QModelIndex& child = model.index(row, 0, index);

        if (Path path = info_provider.repositoryPath(child); !path.isEmpty())
            paths.push_back(path);

        retrieveAllAny(model, child, info_provider, paths);
    }
}

PathList retrieveAllAny(const QAbstractItemView& view,
                        const PathSelector::InfoProvider& info_provider)
{
    if (auto model = view.model())
    {
        PathList paths;

        retrieveAllAny(*model, view.rootIndex(), info_provider, paths);

        return paths;
    }

    return {};
}

PathList retrieveSelectedAny(const QItemSelectionModel&        selection,
                             const PathSelector::InfoProvider& info_provider)
{
    PathList paths;

    const QModelIndexList rows = selection.selectedRows();

    for (const auto& row : rows)
        if (Path path = info_provider.repositoryPath(row); !path.isEmpty())
            paths.push_back(path);

    return paths;
}

PathList retrieveSelectedAny(const QAbstractItemView& view,
                             const PathSelector::InfoProvider& info_provider)
{
    if (auto selection = view.selectionModel())
        return retrieveSelectedAny(*selection, info_provider);

    return {};
}

PathList retrieveSelectedRoot(const QItemSelectionModel&        selection,
                              const PathSelector::InfoProvider& info_provider)
{
    PathList paths;

    const QModelIndexList rows = selection.selectedRows();

    for (const auto& row : rows)
        if (!row.parent().isValid())
            if (Path path = info_provider.repositoryPath(row); !path.isEmpty())
                paths.push_back(path);

    return paths;
}

PathList retrieveSelectedRoot(const QAbstractItemView& view,
                              const PathSelector::InfoProvider& info_provider)
{
    if (auto selection = view.selectionModel())
        return retrieveSelectedRoot(*selection, info_provider);

    return {};
}

} // namespace

PathList PathSelector::repositories(PathSelector::Mode mode) const
{
    PathList paths;

    switch (mode)
    {
    case Mode::AllAny:
        paths = retrieveAllAny(m_view, m_info_provider);
        break;
    case Mode::SelectedAny:
        paths = retrieveSelectedAny(m_view, m_info_provider);
        break;
    case Mode::SelectedRoot:
        paths = retrieveSelectedRoot(m_view, m_info_provider);
        break;
    }

    return paths;
}

} // namespace GitComplex::Repository::Selector::View::Detail
