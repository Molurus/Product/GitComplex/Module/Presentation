/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Selector/View/Detail/IdSelector.h>

/*!
 * \file src/GitComplex/Repository/Selector/View/Detail/IdSelector.cpp
 * \brief src/GitComplex/Repository/Selector/View/Detail/IdSelector.cpp
 */

namespace GitComplex::Repository::Selector::View::Detail
{

namespace
{

void retrieveAllAny(const QAbstractItemModel&       model,
                    const QModelIndex&              index,
                    const IdSelector::InfoProvider& info_provider,
                    IdList& ids)
{
    int row_count = model.rowCount(index);
    for (int row = 0; row < row_count; ++row)
    {
        const QModelIndex& child = model.index(row, 0, index);

        if (OptionalId id = info_provider.repositoryId(child))
            ids.push_back(*id);

        retrieveAllAny(model, child, info_provider, ids);
    }
}

IdList retrieveAllAny(const QAbstractItemView&        view,
                      const IdSelector::InfoProvider& info_provider)
{
    if (auto model = view.model())
    {
        IdList ids;

        retrieveAllAny(*model, view.rootIndex(), info_provider, ids);

        return ids;
    }

    return {};
}

IdList retrieveSelectedAny(const QItemSelectionModel&      selection,
                           const IdSelector::InfoProvider& info_provider)
{
    IdList ids;

    const QModelIndexList rows = selection.selectedRows();

    for (const auto& row : rows)
        if (OptionalId id = info_provider.repositoryId(row))
            ids.push_back(*id);

    return ids;
}

IdList retrieveSelectedAny(const QAbstractItemView&        view,
                           const IdSelector::InfoProvider& info_provider)
{
    if (auto selection = view.selectionModel())
        return retrieveSelectedAny(*selection, info_provider);

    return {};
}

} // namespace

IdList IdSelector::repositories(Mode mode) const
{
    switch (mode)
    {
    case Mode::AllAny:
        return retrieveAllAny(m_view, m_info_provider);
    case Mode::SelectedAny:
        return retrieveSelectedAny(m_view, m_info_provider);
    case Mode::SelectedRoot:
        return {};
    }

    return {};
}

} // namespace GitComplex::Repository::Selector::View::Detail
