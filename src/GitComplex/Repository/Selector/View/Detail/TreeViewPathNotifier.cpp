/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Selector/View/Detail/TreeViewPathNotifier.h>

#include <GitComplex/Integration/Otl/Qt.h>

/*!
 * \file src/GitComplex/Repository/Selector/View/Detail/TreeViewPathNotifier.cpp
 * \brief src/GitComplex/Repository/Selector/View/Detail/TreeViewPathNotifier.cpp
 */

namespace GitComplex::Repository::Selector::View::Detail
{

namespace
{

bool hasSelectedRoot(const QItemSelectionModel& selection)
{
    const QModelIndexList rows = selection.selectedRows();
    return std::any_of(rows.begin(), rows.end(), [](auto& row)
                       { return !row.parent().isValid(); });
}

FixedObjectConnections<4> connect(
    const QAbstractItemView& view,
    const QPointer<TreeViewPathNotifier::EventDelegate>& event_delegate)
{
    QAbstractItemModel* model = view.model();
    assert(model);

    QItemSelectionModel* selection = view.selectionModel();
    assert(selection);

    return {
        QObject::connect(model, &QAbstractItemModel::rowsInserted,
                         [ = ](const QModelIndex&, int, int)
                {
                    if (event_delegate)
                        (*event_delegate).hasRepositoriesChanged(true);
                }),
        QObject::connect(model, &QAbstractItemModel::rowsRemoved,
                         [ = ](const QModelIndex&, int, int)
                {
                    if (event_delegate)
                        (*event_delegate).hasRepositoriesChanged(
                            (*model).rowCount() > 0);
                }),
        QObject::connect(model, &QAbstractItemModel::modelReset,
                         [ = ]()
                {
                    if (event_delegate)
                        (*event_delegate).hasRepositoriesChanged(
                            (*model).rowCount() > 0);
                }),
        QObject::connect(selection, &QItemSelectionModel::selectionChanged,
                         [ = ](const QItemSelection&, const QItemSelection&)
                {
                    if (event_delegate)
                        (*event_delegate).hasRepositoriesChanged(
                            hasSelectedRoot(*selection));
                })
    };
}

} // namespace

TreeViewPathNotifier::TreeViewPathNotifier(const QAbstractItemView& view)
    : m_event_delegate{otn::itself},
      m_connections{connect(view, m_event_delegate)}
{}

} // namespace GitComplex::Repository::Selector::View::Detail
