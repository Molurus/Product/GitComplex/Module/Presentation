/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/RootList/Presenter/Actions/Detail/Editor.h>

#include <GitComplex/Integration/Otl/Qt.h>

/*!
 * \file src/GitComplex/Repository/RootList/Presenter/Actions/Detail/Editor.cpp
 * \brief src/GitComplex/Repository/RootList/Presenter/Actions/Detail/Editor.cpp
 */

namespace GitComplex::Repository::RootList::Presenter::Actions::Detail
{

namespace
{

FixedObjectConnections<1> connect(
    const Interactor::Detail::Editor& editor,
    const Actions::Reference&         remove_repository_action)
{
    using ProviderEventDelegate =
        Interactor::Detail::Editor::ProviderEventDelegate;

    return {
        QObject::connect(&editor.hasRepositoriesToRemoveChanged(),
                         &ProviderEventDelegate::hasRepositoriesChanged,
                         remove_repository_action,
                         &QAction::setEnabled)
    };
}

void connect(
    const Actions::Reference& add_repository_action,
    const Actions::Reference& remove_repository_action,
    otn::weak_optional<Interactor::Detail::Editor> editor)
{
    QObject::connect(add_repository_action, &QAction::triggered,
                     [ = ]
            {
                otn::access(editor, [&](auto& editor)
                            { editor.addRepositories(); });
            });
    QObject::connect(remove_repository_action, &QAction::triggered,
                     [editor = std::move(editor)]
            {
                otn::access(editor, [&](auto& editor)
                            { editor.removeRepositories(); });
            });
}

} // namespace

Editor::Editor(const InteractorEditor& editor,
               otn::weak_optional<InteractorEditor> weak_editor)
    : m_add_repository{otn::itself},
      m_remove_repositories{otn::itself},
      m_connections{connect(editor, m_remove_repositories)}
{
    setup();
    connect(m_add_repository, m_remove_repositories, std::move(weak_editor));
}

void Editor::setup()
{
    (*m_add_repository).setText(QAction::tr("Open Repository..."));
    (*m_add_repository).setShortcut(QKeySequence(::Qt::CTRL + ::Qt::Key_O));
    (*m_add_repository).setIcon(QIcon::fromTheme(QStringLiteral("document-open")));

    (*m_remove_repositories).setText(QAction::tr("Close Repositories..."));
    (*m_remove_repositories).setEnabled(false);
}

Editor::ActionSet Editor::actions() const
{
    return {m_add_repository,
            m_remove_repositories};
}

} // namespace GitComplex::Repository::RootList::Presenter::Actions::Detail
