/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Synchronizer/Presenter/Actions/Detail/TreeStructure.h>

#include <GitComplex/Integration/Otl/Qt.h>

/*!
 * \file src/GitComplex/Repository/Synchronizer/Presenter/Actions/Detail/TreeStructure.cpp
 * \brief src/GitComplex/Repository/Synchronizer/Presenter/Actions/Detail/TreeStructure.cpp
 */

namespace GitComplex::Repository::Synchronizer::Presenter::Actions::Detail
{

namespace
{

void connect(QAction& action,
             const otn::weak_optional<Entity::TreeStructure>& tree_structure)
{
    QObject::connect(&action, &QAction::triggered, [ = ]()
            {
                otn::access(tree_structure, [&](auto& tree_structure)
                            { tree_structure.synchronizeStructure(); });
            });
}

} // namespace

TreeStructure::TreeStructure(
    const otn::weak_optional<Entity::TreeStructure>& tree_structure)
    : m_synchronize_structure{otn::itself}
{
    setup();
    connect(*m_synchronize_structure, tree_structure);
}

void TreeStructure::setup()
{
    (*m_synchronize_structure).setText(QAction::tr("Scan"));
    (*m_synchronize_structure).setToolTip(QAction::tr("Scan Repositories Structure"));
    (*m_synchronize_structure).setShortcut(QKeySequence(::Qt::SHIFT + ::Qt::Key_F5));
}

TreeStructure::ActionSet TreeStructure::actions() const
{
    return {m_synchronize_structure};
}

} // namespace GitComplex::Repository::Synchronizer::Presenter::Actions::Detail
