/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Synchronizer/Presenter/Actions/Detail/TreeStatus.h>

#include <GitComplex/Integration/Otl/Qt.h>

/*!
 * \file src/GitComplex/Repository/Synchronizer/Presenter/Actions/Detail/TreeStatus.cpp
 * \brief src/GitComplex/Repository/Synchronizer/Presenter/Actions/Detail/TreeStatus.cpp
 */

namespace GitComplex::Repository::Synchronizer::Presenter::Actions::Detail
{

namespace
{

void connect(QAction& action,
             const otn::weak_optional<Entity::TreeStatus>& tree_status)
{
    QObject::connect(&action, &QAction::triggered, [ = ]()
            {
                using Selection::Mode;
                otn::access(tree_status, [&](auto& tree_status)
                            { tree_status.updateStatus(Mode::SelectedAny); });
            });
}

} // namespace

TreeStatus::TreeStatus(const otn::weak_optional<Entity::TreeStatus>& tree_status)
    : m_update_status{otn::itself}
{
    setup();
    connect(*m_update_status, tree_status);
}

void TreeStatus::setup()
{
    (*m_update_status).setText(QAction::tr("Update"));
    (*m_update_status).setToolTip(
        QAction::tr("Update Status of Selected Repositories"));
    (*m_update_status).setShortcut(QKeySequence::Refresh);
}

TreeStatus::ActionSet TreeStatus::actions() const
{
    return {m_update_status};
}

} // namespace GitComplex::Repository::Synchronizer::Presenter::Actions::Detail
