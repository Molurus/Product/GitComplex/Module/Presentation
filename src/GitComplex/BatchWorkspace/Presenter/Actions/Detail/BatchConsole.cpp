/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/BatchWorkspace/Presenter/Actions/Detail/BatchConsole.h>

#include <GitComplex/Integration/Otl/Qt.h>

/*!
 * \file src/GitComplex/BatchWorkspace/Presenter/Actions/Detail/BatchConsole.cpp
 * \brief src/GitComplex/BatchWorkspace/Presenter/Actions/Detail/BatchConsole.cpp
 */

namespace GitComplex::BatchWorkspace::Presenter::Actions::Detail
{

namespace
{

void connect(QAction& action,
             const otn::weak_optional<Interactor::BatchConsole>& console)
{
    QObject::connect(&action, &QAction::triggered, [ = ]()
            {
                otn::access(console, [&](auto& console)
                            { console.clearCurrentOutput(); });
            });
}

} // namespace

BatchConsole::BatchConsole(
    const otn::weak_optional<Interactor::BatchConsole>& console)
    : m_clear_output{otn::itself}
{
    setup();
    connect(*m_clear_output, console);
}

void BatchConsole::setup()
{
    (*m_clear_output).setText(QAction::tr("Clear Output"));
    (*m_clear_output).setToolTip(QAction::tr("Clear Shell Command Output"));
}

BatchConsole::ActionSet BatchConsole::actions() const
{
    return {m_clear_output};
}

} // namespace GitComplex::BatchWorkspace::Presenter::Actions::Detail
