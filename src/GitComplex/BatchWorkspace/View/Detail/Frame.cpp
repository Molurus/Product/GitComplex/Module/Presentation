﻿/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/BatchWorkspace/View/Detail/Frame.h>

/*!
 * \file src/GitComplex/BatchWorkspace/View/Detail/Frame.cpp
 * \brief src/GitComplex/BatchWorkspace/View/Detail/Frame.cpp
 */

namespace GitComplex::BatchWorkspace::View::Detail
{

void Frame::save(Settings::RepositoryStorage& storage) const
{
    storage.setValue("State", saveState());
}

void Frame::load(Settings::RepositoryStorage& storage)
{
    restoreState(storage.value("State").toByteArray());
}

void Frame::emplaceWidget(Widget::Slot<View::ObserverPlaceholder> observer)
{
    m_observer_placeholder = std::move(observer);
    if (m_observer_placeholder)
        addWidget(m_observer_placeholder);
}

void Frame::emplaceWidget(Slot::ConsolePlaceholder console)
{
    m_console_placeholder = std::move(console);
    if (m_console_placeholder)
        addWidget(m_console_placeholder);
}

void Frame::emplaceActions(const RootListEditorActions& actions)
{
    if (m_observer_placeholder)
        (*m_observer_placeholder).emplaceActions(actions);
}

} // namespace GitComplex::BatchWorkspace::View::Detail
