/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "ui_ConsolePlaceholder.h"

#include <GitComplex/BatchWorkspace/View/Detail/ConsolePlaceholder.h>
#include <GitComplex/Settings/Utility.h>

/*!
 * \file src/GitComplex/BatchWorkspace/View/Detail/ConsolePlaceholder.cpp
 * \brief src/GitComplex/BatchWorkspace/View/Detail/ConsolePlaceholder.cpp
 */

namespace GitComplex::BatchWorkspace::View::Detail
{

ConsolePlaceholder::ConsolePlaceholder(QWidget* parent, ::Qt::WindowFlags flags)
    : View::ConsolePlaceholder{parent, flags},
      ui{new Ui::ConsolePlaceholderWidget()}
{
    ui->setupUi(this);

    setupUi();
}

ConsolePlaceholder::~ConsolePlaceholder()
{
    delete ui;
}

void ConsolePlaceholder::setupUi()
{
    setupToolBar();
}

void ConsolePlaceholder::setupToolBar()
{
    QToolBar* toolbar = new QToolBar(this);

    ui->layout->insertWidget(0, toolbar);
}

void ConsolePlaceholder::save(Settings::RepositoryStorage& storage) const
{
    if (m_console_array)
        Settings::save_pmr(*m_console_array, storage, "ConsoleArrayWidget");
}

void ConsolePlaceholder::load(Settings::RepositoryStorage& storage)
{
    if (m_console_array)
        Settings::load_pmr(*m_console_array, storage, "ConsoleArrayWidget");
}

void ConsolePlaceholder::emplaceWidget(Slot::ConsoleArray console_array)
{
    m_console_array = std::move(console_array);
    ui->layout->addWidget(m_console_array);
}

void ConsolePlaceholder::emplaceWidget(Slot::SelectionSwitcher switcher)
{
    m_selection_switcher = std::move(switcher);
    toolBar().insertWidget(m_batch_console_actions.clearOutput,
                           m_selection_switcher);
}

void ConsolePlaceholder::emplaceActions(const BatchConsoleActions& actions)
{
    m_batch_console_actions = actions;

    toolBar().addAction(actions.clearOutput);
}

QToolBar& ConsolePlaceholder::toolBar()
{
    return static_cast<QToolBar&>(*ui->layout->itemAt(0)->widget());
}

} // namespace GitComplex::BatchWorkspace::View::Detail
