/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "ui_ObserverPlaceholder.h"

#include <GitComplex/BatchWorkspace/View/Detail/ObserverPlaceholder.h>

/*!
 * \file src/GitComplex/BatchWorkspace/View/Detail/ObserverPlaceholder.cpp
 * \brief src/GitComplex/BatchWorkspace/View/Detail/ObserverPlaceholder.cpp
 */

namespace GitComplex::BatchWorkspace::View::Detail
{

ObserverPlaceholder::ObserverPlaceholder(QWidget* parent, ::Qt::WindowFlags flags)
    : View::ObserverPlaceholder{parent, flags},
      ui{new Ui::RepositoryObserverPlaceholderWidget},
      m_editor_menu{nullptr}
{
    ui->setupUi(this);

    setupUi();
}

ObserverPlaceholder::~ObserverPlaceholder()
{
    delete ui;
}

void ObserverPlaceholder::setupUi()
{
    setupSynchronizerToolBar();
    setupEditorMenu();
}

void ObserverPlaceholder::setupSynchronizerToolBar()
{
    auto* toolbar = new QToolBar(this);

    ui->layout->insertWidget(0, toolbar);
}

void ObserverPlaceholder::setupEditorMenu()
{
    m_editor_menu = new QMenu(this);
}

void ObserverPlaceholder::emplaceWidget(Slot::GenericObserver observer)
{
    m_observer = std::move(observer);
    ui->layout->addWidget(m_observer);
    connect(m_observer, &QWidget::customContextMenuRequested,
            this, &ObserverPlaceholder::showEditorMenu);
}

void ObserverPlaceholder::emplaceActions(
    const RootListEditorActions& actions)
{
    m_root_list_editor_actions = actions;

    editorMenu().addAction(m_root_list_editor_actions.addRepository);
    editorMenu().addAction(m_root_list_editor_actions.removeRepositories);
}

void ObserverPlaceholder::emplaceActions(
    const SynchronizerActions::TreeStructure& actions)
{
    m_tree_structure_synchronizer_actions = actions;

    synchronizerToolBar().addAction(actions.synchronizeStructure);
}

void ObserverPlaceholder::emplaceActions(
    const SynchronizerActions::TreeStatus& actions)
{
    m_tree_status_synchronizer_actions = actions;

    synchronizerToolBar().addAction(actions.updateStatusOfSelected);
}

void ObserverPlaceholder::showEditorMenu(const QPoint& position_in_observer)
{
    if (m_observer)
        editorMenu().exec((*m_observer).mapToGlobal(position_in_observer));
}

QToolBar& ObserverPlaceholder::synchronizerToolBar()
{
    return static_cast<QToolBar&>(*ui->layout->itemAt(0)->widget());
}

} // namespace GitComplex::BatchWorkspace::View::Detail
