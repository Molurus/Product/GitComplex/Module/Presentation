/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Console/View/Detail/StackedSplitter.h>

/*!
 * \file src/GitComplex/Console/View/Detail/StackedSplitter.cpp
 * \brief src/GitComplex/Console/View/Detail/StackedSplitter.cpp
 */

namespace GitComplex::Console::View::Detail
{

void StackedSplitter::setCurrentIndex(int index)
{
    if (index == m_current_index)
        return;

    for (int i = 0; i < count(); ++i)
        static_cast<QStackedWidget*>(widget(i))->setCurrentIndex(index);

    m_current_index = index;
}

void StackedSplitter::save(Settings::RepositoryStorage& storage) const
{
    storage.setValue("Splitter", saveState());
}

void StackedSplitter::load(Settings::RepositoryStorage& storage)
{
    restoreState(storage.value("Splitter").toByteArray());
}

} // namespace GitComplex::Console::View::Detail
