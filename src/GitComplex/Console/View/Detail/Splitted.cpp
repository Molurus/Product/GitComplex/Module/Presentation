/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Console/View/Detail/Splitted.h>

#include <GitComplex/Integration/Otl/Qt.h>

/*!
 * \file src/GitComplex/Console/View/Detail/Splitted.cpp
 * \brief src/GitComplex/Console/View/Detail/Splitted.cpp
 */

namespace GitComplex::Console::View::Detail
{

Splitted::Splitted(Widget::Reference<Editor>     editor,
                   Widget::Reference<OutputView> output)
    : m_editor{std::move(editor)},
      m_output{std::move(output)},
      m_event_delegate{otn::itself}
{
    assert(m_editor);

    adjustHasTarget(false);

    QObject::connect(m_editor,
                     &Editor::commandExecutionRequested,
                     static_cast<SingleEventDelegate*>(m_event_delegate),
                     &SingleEventDelegate::commandExecutionRequested);
}

Command::Line Splitted::currentCommand() const
{
    if (m_editor)
        return m_editor->currentCommand();

    return {};
}

void Splitted::printCommandSummaries(const Command::SummaryList& summaries)
{
    if (m_output)
        m_output->print(summaries);
}

void Splitted::clearOutput()
{
    if (m_output)
        m_output->clear();
}

void Splitted::adjustHasTarget(bool has_target)
{
    if (m_editor)
    {
        if (has_target)
            m_editor->setStyleSheet("color: black");
        else
            m_editor->setStyleSheet("color: grey");
    }
}

void Splitted::save(Settings::RepositoryStorage& storage) const
{
    storage.beginGroup("Editor");

    if (m_editor)
        m_editor->save(storage);

    storage.endGroup();
}

void Splitted::load(Settings::RepositoryStorage& storage)
{
    storage.beginGroup("Editor");

    if (m_editor)
        m_editor->load(storage);

    storage.endGroup();
}

} // namespace GitComplex::Console::View::Detail
