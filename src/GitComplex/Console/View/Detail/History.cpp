/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Console/View/Detail/History.h>

/*!
 * \file src/GitComplex/Console/View/Detail/History.cpp
 * \brief src/GitComplex/Console/View/Detail/History.cpp
 */

namespace GitComplex::Console::View::Detail
{

void History::appendCommand(const Command::Line& command)
{
    if (isEmpty() || last() != command)
    {
        append(command);
        removeExcessCommands();
    }
}

Command::Line History::previousCommand()
{
    if (!m_is_browsing)
    {
        return startBrowse();
    }
    else
    {
        if (current_iterator != crend() - 1)
            ++current_iterator;

        return *current_iterator;
    }
}

Command::Line History::nextCommand()
{
    if (current_iterator != crbegin())
    {
        return *(--current_iterator);
    }
    else
    {
        resetBrowse();
        return QString{};
    }
}

void History::resetBrowse()
{
    m_is_browsing = false;
}

void History::save(Settings::RepositoryStorage& storage) const
{
    storage.setValue("Commands", *this);
}

void History::load(Settings::RepositoryStorage& storage)
{
    QStringList::operator=(storage.value("Commands").toStringList());
    removeExcessCommands();
}

Command::Line History::startBrowse()
{
    m_is_browsing    = true;
    current_iterator = crbegin();
    return *current_iterator;
}

void History::removeExcessCommands()
{
    while (count() > MaxCount)
        removeAt(0);
}

} // namespace GitComplex::Console::View::Detail
