/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Console/View/Detail/OutputView.h>

#include <QStringBuilder>

/*!
 * \file src/GitComplex/Console/View/Detail/OutputView.cpp
 * \brief src/GitComplex/Console/View/Detail/OutputView.cpp
 */

namespace GitComplex::Console::View::Detail
{

OutputView::OutputView(QWidget* parent)
    : QTextEdit{parent}
{
    setReadOnly(true);

    setupFormats();
}

void OutputView::setupFormats()
{
    m_command_format.setFontWeight(QFont::Bold);
    m_error_format.setForeground(::Qt::red);
    m_exit_status_format.setForeground(::Qt::red);
    m_output_error_format.setForeground(::Qt::darkRed);
}

void OutputView::print(const Command::SummaryList& summaries)
{
    if (m_append_place == AppendPlace::Top)
        moveCursor(QTextCursor::Start);

    for (auto last = --summaries.cend(), current = summaries.cbegin();
         current != summaries.cend(); ++current)
    {
        print(*current);
        if (current != last)
            insertPlainText("\n\n");
    }
    printSeparator();

    if (m_append_place == AppendPlace::Bottom)
        moveCursor(QTextCursor::End);
}

void OutputView::print(const Command::Summary& summary)
{
    print(summary.workingDirectory, m_working_directory_format);
    print("\n> " % summary.command, m_command_format);
    printError(summary);
    printOutputLines(summary.output.result, m_output_result_format);
    printOutputLines(summary.output.error,  m_output_error_format);
    printExitStatus(summary);
}

void OutputView::printOutputLines(const Command::Output::Lines& text,
                                  const QTextCharFormat&        format)
{
    if (!text.isEmpty())
        print("\n" % text.join("\n").trimmed(), format);
}

void OutputView::printError(const Command::Summary& summary)
{
    if (summary.error)
        print(QString{"\n%1 error (%2): %3"}.
                arg(QString::fromStdString(summary.error.category().name())).
                arg(summary.error.value()).
                arg(QString::fromStdString(summary.error.message())),
              m_exit_status_format);
}

void OutputView::printExitStatus(const Command::Summary& summary)
{
    if (summary.exitStatus != 0)
        print("\nExit status: " % QString::number(summary.exitStatus),
              m_exit_status_format);
}

void OutputView::print(const QString& text, const QTextCharFormat& format)
{
    textCursor().insertText(text, format);
}

void OutputView::printSeparator()
{
    insertHtml("<br><hr><br>");
}

} // namespace GitComplex::Console::View::Detail
