/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Console/View/Detail/Editor.h>

/*!
 * \file src/GitComplex/Console/View/Detail/Editor.cpp
 * \brief src/GitComplex/Console/View/Detail/Editor.cpp
 */

namespace GitComplex::Console::View::Detail
{

Editor::Editor(QWidget* parent)
    : QPlainTextEdit{parent}
{}

void Editor::save(Settings::RepositoryStorage& storage) const
{
    storage.beginGroup("History");
    m_history.save(storage);
    storage.endGroup();
}

void Editor::load(Settings::RepositoryStorage& storage)
{
    storage.beginGroup("History");
    m_history.load(storage);
    storage.endGroup();
}

Command::Line Editor::currentCommand()
{
    return prepareCommand();
}

void Editor::keyPressEvent(QKeyEvent* event)
{
    switch (event->key())
    {
    case ::Qt::Key_Enter:
    case ::Qt::Key_Return:
        executeCurrentCommand();
        appendCurrentCommandToHistory();
        clearCurrentCommand();
        event->accept();
        return;
    case ::Qt::Key_Up:
        browseHistoryPrevious();
        event->accept();
        return;
    case ::Qt::Key_Down:
        browseHistoryNext();
        event->accept();
        return;
    } // switch

    QPlainTextEdit::keyPressEvent(event);
}

void Editor::mouseDoubleClickEvent(QMouseEvent* event)
{
    if (textCursor().atEnd())
    {
        executeCurrentCommand();

        event->accept();
    }
    else
    {
        QPlainTextEdit::mouseDoubleClickEvent(event);
    }
}

void Editor::wheelEvent(QWheelEvent* event)
{
    if (event->angleDelta().y() == 0)
    {
        QPlainTextEdit::wheelEvent(event);
        return;
    }

    if (event->angleDelta().y() > 0)
        browseHistoryPrevious();
    else
        browseHistoryNext();

    event->accept();
}

Command::Line Editor::prepareCommand()
{
    return toPlainText();
}

void Editor::executeCommand(const Command::Line& command)
{
    if (command.isEmpty())
        return;

    emit commandExecutionRequested(command);
}

void Editor::executeCurrentCommand()
{
    Command::Line command = prepareCommand();
    executeCommand(command);
}

void Editor::appendCurrentCommandToHistory()
{
    Command::Line command = prepareCommand();
    m_history.appendCommand(command);
}

void Editor::clearCurrentCommand()
{
    m_history.resetBrowse();
    clear();
}

void Editor::browseHistoryPrevious()
{
    if (!m_history.isEmpty())
    {
        setPlainText(m_history.previousCommand());
        moveCursor(QTextCursor::End);
    }
}

void Editor::browseHistoryNext()
{
    if (m_history.isBrowsing())
    {
        setPlainText(m_history.nextCommand());
        moveCursor(QTextCursor::End);
    }
}

} // namespace GitComplex::Console::View::Detail
