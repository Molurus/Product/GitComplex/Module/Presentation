/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Shell/Entity/Qt/ToolLauncher.h>
#include <GitComplex/Repository/Tree/Presenter/InfoProvider.h>

#include <QModelIndex>

#include <otn/all.hpp>

#include <GitComplex/Presentation/Export.h>

/*!
 * \file include/GitComplex/Repository/Tree/Presenter/Detail/ToolLauncher.h
 * \brief include/GitComplex/Repository/Tree/Presenter/Detail/ToolLauncher.h
 */

namespace GitComplex::Repository::Tree::Presenter::Detail
{

/*!
 * \class ToolLauncher
 * \brief ToolLauncher.
 *
 * \headerfile GitComplex/Repository/Tree/Presenter/Detail/ToolLauncher.h
 *
 * ToolLauncher.
 *
 * \note [[Abstraction::Detail]] [[Viper::Presenter]] [[Framework::Qt]]
 */
class GITCOMPLEX_PRESENTATION_EXPORT
        ToolLauncher
{
public:
    using ShellToolLauncher = Repository::Shell::Entity::Qt::ToolLauncher;

    ToolLauncher(otn::raw::weak_single<const ShellToolLauncher> tool_launcher,
                 otn::weak_optional<const InfoProvider>         info_provider)
        : m_tool_launcher{std::move(tool_launcher)},
          m_info_provider{std::move(info_provider)}
    {}

    void launch(const QModelIndex& index) const;

private:
    otn::raw::weak_single<const ShellToolLauncher> m_tool_launcher;
    otn::weak_optional<const InfoProvider> m_info_provider;
};

} // namespace GitComplex::Repository::Tree::Presenter::Detail
