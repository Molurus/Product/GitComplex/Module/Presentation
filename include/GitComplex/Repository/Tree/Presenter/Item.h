/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Core/Qt/Path.h>
#include <GitComplex/Repository/Entity/Qt/Id.h>
#include <GitComplex/Repository/Entity/Qt/DirectoryState.h>
#include <GitComplex/Repository/Entity/Qt/Branch.h>
#include <GitComplex/Repository/Entity/Qt/Sha.h>
#include <GitComplex/Repository/Tree/Presenter/Namespace.h>

#include <otn/all.hpp>

/*!
 * \file include/GitComplex/Repository/Tree/Presenter/Item.h
 * \brief include/GitComplex/Repository/Tree/Presenter/Item.h
 */

namespace GitComplex::Repository::Tree::Presenter
{

/*!
 * \class Item
 * \brief Item.
 *
 * \headerfile GitComplex/Repository/Tree/Presenter/Item.h
 *
 * Item.
 *
 * \note [[using Abstraction : Detail, Concrete]]
 *       [[Viper::Presenter]] [[Framework::Qt]]
 */
class Item
{
public:
    using WeakOptional      = otn::raw::weak_optional<Item>;
    using WeakOptionalConst = otn::raw::weak_optional<const Item>;
    using WeakSingle        = otn::raw::weak_single<Item>;
    using WeakSingleConst   = otn::raw::weak_single<const Item>;

    Item(SingleId       id,
         Path           path,
         DirectoryState directory_state,
         OptionalId     superproject_id,
         WeakOptional   parent = {})
        : m_id{std::move(id)},
          m_path{std::move(path)},
          m_directory_state{directory_state},
          m_superproject_id{std::move(superproject_id)},
          m_parent{std::move(parent)},
          m_submodule_path{extractSubmodulePath()}
    {}

    void addSubmodule(otn::slim::unique_single<Item> submodule)
    { m_submodules.push_back(std::move(submodule)); }

    void clearSubmodules()      { m_submodules.clear(); }

    WeakOptional parent() const { return m_parent; }

    std::size_t  index() const;

    // Getters.
    const SingleId& id() const      { return m_id; }
    Path   path() const { return m_path; }
    Path   submodulePath() const    { return m_submodule_path; }
    DirectoryState directoryState() const { return m_directory_state; }
    Branch branch() const           { return m_branch; }
    bool   hasModifications() const { return m_has_modifications; }
    bool   isChecked() const        { return m_is_checked; }

    bool   isSuperproject() const   { return !submodules().empty(); }
    bool   isSubmodule() const      { return static_cast<bool>(m_superproject_id); }

    // Setters.
    void setBranch(const Branch& branch)   { m_branch = branch; }
    void setHasModifications(bool has_mod) { m_has_modifications = has_mod; }

    /*!
     * \class List
     * \brief List.
     *
     * \headerfile GitComplex/Repository/Tree/Presenter/Item.h
     *
     * List.
     */
    class List : public std::vector<otn::slim::unique_single<Item>>
    {
    public:
        using ItemToken = otn::slim::unique_single<Item>;

        IdList       merge(List other);
        IdList       remove(const IdList& ids);

        WeakOptional item(std::size_t row) const { return at(row); }
        WeakOptional findItem(const SingleId& id) const;
    };

    const List&  submodules() const
    { return m_submodules; }
    WeakOptional submodule(std::size_t index) const
    { return m_submodules.at(index); }

protected:
    Path extractSubmodulePath() const;

private:
    SingleId       m_id;
    Path           m_path;
    DirectoryState m_directory_state;
    OptionalId     m_superproject_id;
    WeakOptional   m_parent;
    Path           m_submodule_path;

    List           m_submodules;

    Sha    m_sha;
    Branch m_branch;
    bool   m_has_modifications{false};
    bool   m_is_checked{false};
};

} // namespace GitComplex::Repository::Tree::Presenter
