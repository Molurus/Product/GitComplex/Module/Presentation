/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Tree/Entity/Qt/Observer.h>
#include <GitComplex/Repository/Tree/Entity/Qt/DependenceProvider.h>
#include <GitComplex/Repository/Tree/Presenter/Item.h>
#include <GitComplex/Repository/Tree/Presenter/InfoProvider.h>

#include <QAbstractItemModel>

#include <GitComplex/Presentation/Export.h>

/*!
 * \file include/GitComplex/Repository/Tree/Presenter/Model.h
 * \brief include/GitComplex/Repository/Tree/Presenter/Model.h
 */

namespace GitComplex::Repository::Tree::Presenter
{

/*!
 * \class Model
 * \brief Model.
 *
 * \headerfile GitComplex/Repository/Tree/Presenter/Model.h
 *
 * Model.
 *
 * \note [[using Abstraction : Detail, Concrete]]
 *       [[Viper::Presenter]] [[Framework::Qt]]
 */
class GITCOMPLEX_PRESENTATION_EXPORT
        Model final : public QAbstractItemModel,
                      public Observer,
                      public DependenceProvider,
                      public InfoProvider
{
    Q_OBJECT

public:
    explicit Model(QObject* parent = nullptr);

private:
    // QAbstractItemModel
    int rowCount(const QModelIndex& parent    = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex     index(int row, int column,
                          const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex     parent(const QModelIndex& index) const override;
    ::Qt::ItemFlags flags(const QModelIndex& index) const override;
    QVariant data(const QModelIndex& index,
                  int role = ::Qt::DisplayRole) const override;
    bool     setData(const QModelIndex& index, const QVariant& value,
                     int role = ::Qt::EditRole) override;
    QVariant headerData(int section, ::Qt::Orientation orientation,
                        int role = ::Qt::DisplayRole) const override;

    // StructureObserver
    void resetRepositories(Structure::List structures = {}) override;
    void addRepositories(Structure::List structures) override;
    void removeRepositories(IdList ids) override;

    // StatusObserver
    void updateRepositoriesStatus(StatusList statuses) override;
    void updateRepositoryStatus(const Status& status);

    // DependenceProvider
    IdList dependentSuperprojects(const IdList& ids) const override;
    IdList dependentSubmodules(const IdList& ids) const override;

    // StructureObservable
    Delegate& repositoriesAdded() const override
    { return *m_structure_observable_delegate; }
    Delegate& repositoriesRemoved() const override
    { return *m_structure_observable_delegate; }

    // InfoProvider
    OptionalId repositoryId(const QModelIndex& index) const override;
    Path       repositoryPath(const QModelIndex& index) const override;

    // Other
    bool hasItems() const { return !m_items.empty(); }

    // Data
    Item::List m_items;
    otn::slim::unique_single<Delegate> m_structure_observable_delegate;
};

} // namespace GitComplex::Repository::Tree::Presenter
