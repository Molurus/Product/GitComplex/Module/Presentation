/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <QTreeView>
#include <QTimer>

#include <GitComplex/Presentation/Export.h>

// NOTE: The name of the file is different from the name of the class
// due to bugs like QBS-1380.
/*!
 * \file include/GitComplex/Repository/Tree/View/Detail/TreeObserver.h
 * \brief include/GitComplex/Repository/Tree/View/Detail/TreeObserver.h
 */

namespace GitComplex::Repository::Tree::View::Detail
{

/*!
 * \class Observer
 * \brief Observer.
 *
 * \headerfile GitComplex/Repository/Tree/View/Detail/TreeObserver.h
 *
 * Observer.
 *
 * \note [[using Abstraction : Detail, Concrete]]
 *       [[Viper::View]] [[Framework::Qt]]
 */
class GITCOMPLEX_PRESENTATION_EXPORT
        Observer : public QTreeView
{
    Q_OBJECT

public:
    Observer(QAbstractItemModel* model, QWidget* parent = nullptr);

    void reset() override;

protected:
    using QTreeView::setModel;

    void dataChanged(const QModelIndex&  top_left,
                     const QModelIndex&  bottom_right,
                     const QVector<int>& roles = QVector<int>()) override;

    bool hasRepositories() const;

    void adjustInitialView();
    void adjustInitialSelection();

    void adjustSectionsSize();
    void delayAdjustSectionsSize();

private:
    QTimer m_sections_size_timer;
};

} // namespace GitComplex::Repository::Tree::View::Detail
