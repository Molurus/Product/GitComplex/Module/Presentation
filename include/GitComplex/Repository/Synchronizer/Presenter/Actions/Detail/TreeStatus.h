/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Synchronizer/Presenter/Actions/Detail/Namespace.h>
#include <GitComplex/Repository/Synchronizer/Presenter/Actions/TreeStatus.h>
#include <GitComplex/Repository/Synchronizer/Entity/TreeStatus.h>

#include <otn/all.hpp>

#include <GitComplex/Presentation/Export.h>

/*!
 * \file include/GitComplex/Repository/Synchronizer/Presenter/Actions/Detail/TreeStatus.h
 * \brief include/GitComplex/Repository/Synchronizer/Presenter/Actions/Detail/TreeStatus.h
 */

namespace GitComplex::Repository::Synchronizer::Presenter::Actions::Detail
{

/*!
 * \class TreeStatus
 * \brief TreeStatus.
 *
 * \headerfile GitComplex/Repository/Synchronizer/Presenter/Actions/Detail/TreeStatus.h
 *
 * TreeStatus.
 *
 * \note [[Abstraction::Detail]] [[Viper::Presenter]] [[Framework::Qt]]
 */
class GITCOMPLEX_PRESENTATION_EXPORT
        TreeStatus
{
public:
    using ActionSet = Actions::TreeStatus;

    explicit TreeStatus(
        const otn::weak_optional<Entity::TreeStatus>& tree_status);

    ActionSet actions() const;

private:
    void setup();

    Actions::Owner m_update_status;
};

} // namespace GitComplex::Repository::Synchronizer::Presenter::Actions::Detail
