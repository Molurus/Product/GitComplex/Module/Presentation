/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Synchronizer/Presenter/Actions/Detail/Namespace.h>
#include <GitComplex/Repository/Synchronizer/Presenter/Actions/TreeStructure.h>
#include <GitComplex/Repository/Synchronizer/Entity/TreeStructure.h>

#include <otn/all.hpp>

#include <GitComplex/Presentation/Export.h>

/*!
 * \file include/GitComplex/Repository/Synchronizer/Presenter/Actions/Detail/TreeStructure.h
 * \brief include/GitComplex/Repository/Synchronizer/Presenter/Actions/Detail/TreeStructure.h
 */

namespace GitComplex::Repository::Synchronizer::Presenter::Actions::Detail
{

/*!
 * \class TreeStructure
 * \brief TreeStructure.
 *
 * \headerfile GitComplex/Repository/Synchronizer/Presenter/Actions/Detail/TreeStructure.h
 *
 * \note [[Abstraction::Detail]] [[Viper::Presenter]] [[Framework::Qt]]
 */
class GITCOMPLEX_PRESENTATION_EXPORT
        TreeStructure
{
public:
    using ActionSet = Actions::TreeStructure;

    explicit TreeStructure(
        const otn::weak_optional<Entity::TreeStructure>& tree_structure);

    ActionSet actions() const;

private:
    void setup();

    Actions::Owner m_synchronize_structure;
};

} // namespace GitComplex::Repository::Synchronizer::Presenter::Actions::Detail
