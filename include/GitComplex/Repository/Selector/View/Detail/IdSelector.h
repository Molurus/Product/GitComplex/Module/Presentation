/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Selection/Entity/Mode.h>
#include <GitComplex/Repository/Tree/Presenter/InfoProvider.h>
#include <GitComplex/Repository/Selector/View/Detail/Namespace.h>

#include <QAbstractItemView>

/*!
 * \file include/GitComplex/Repository/Selector/View/Detail/IdSelector.h
 * \brief include/GitComplex/Repository/Selector/View/Detail/IdSelector.h
 */

namespace GitComplex::Repository::Selector::View::Detail
{

/*!
 * \class IdSelector
 * \brief IdSelector.
 *
 * \headerfile GitComplex/Repository/Selector/View/Detail/IdSelector.h
 *
 * IdSelector.
 *
 * \note [[Abstraction::Detail]] [[Viper::View]] [[Framework::Qt]]
 */
class IdSelector
{
public:
    using Mode         = Selection::Entity::Mode;
    using InfoProvider = Tree::Presenter::InfoProvider;

    IdSelector(const QAbstractItemView& view,
               const InfoProvider&      info_provider)
        : m_view{view},
          m_info_provider{info_provider}
    {}

    IdList repositories(Mode mode) const;

private:
    const QAbstractItemView& m_view;
    const InfoProvider&      m_info_provider;
};

} // namespace GitComplex::Repository::Selector::View::Detail
