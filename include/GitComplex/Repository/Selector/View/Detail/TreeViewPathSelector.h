/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Tree/Presenter/InfoProvider.h>
#include <GitComplex/Repository/Selector/View/Detail/TreeViewPathNotifier.h>
#include <GitComplex/Repository/Selector/View/Detail/PathConfirmator.h>

#include <QAbstractItemView>
#include <QPointer>

#include <otn/all.hpp>

#include <GitComplex/Presentation/Export.h>

/*!
 * \file include/GitComplex/Repository/Selector/View/Detail/TreeViewPathSelector.h
 * \brief include/GitComplex/Repository/Selector/View/Detail/TreeViewPathSelector.h
 */

namespace GitComplex::Repository::Selector::View::Detail
{

/*!
 * \class TreeViewPathSelector
 * \brief TreeViewPathSelector.
 *
 * \headerfile GitComplex/Repository/Selector/View/Detail/TreeViewPathSelector.h
 *
 * TreeViewPathSelector.
 *
 * \note [[Abstraction::Detail]] [[Viper::View]] [[Framework::Qt]]
 */
class GITCOMPLEX_PRESENTATION_EXPORT
        TreeViewPathSelector
{
public:
    using InfoProvider  = Tree::Presenter::InfoProvider;
    using EventDelegate = TreeViewPathNotifier::EventDelegate;
    using Confirmation  = UserInterface::Dialog::Confirmation;

    TreeViewPathSelector(QPointer<const QAbstractItemView>      view,
                         otn::weak_optional<const InfoProvider> info_provider,
                         Confirmation confirmation = Confirmation::None);

    PathList       repositories() const;

    EventDelegate& hasRepositoriesChanged() const
    { return m_notifier.hasRepositoriesChanged(); }

private:
    QPointer<const QAbstractItemView>      m_view;
    otn::weak_optional<const InfoProvider> m_info_provider;
    PathConfirmator      m_confirmator;
    TreeViewPathNotifier m_notifier;
};

} // namespace GitComplex::Repository::Selector::View::Detail
