/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/RootList/Entity/Provider.h>
#include <GitComplex/Repository/Selector/View/Detail/TreeViewPathSelector.h>

/*!
 * \file include/GitComplex/Repository/Selector/View/Concrete/TreeViewPathSelector.h
 * \brief include/GitComplex/Repository/Selector/View/Concrete/TreeViewPathSelector.h
 */

namespace GitComplex::Repository::Selector::View::Concrete
{

/*!
 * \class TreeViewPathSelector
 * \brief TreeViewPathSelector.
 *
 * \headerfile GitComplex/Repository/Selector/View/Concrete/TreeViewPathSelector.h
 *
 * TreeViewPathSelector.
 *
 * \note [[Abstraction::Concrete]] [[Viper::View]] [[Framework::Qt]]
 */
class TreeViewPathSelector final : public RootList::Entity::Provider
{
public:
    using InfoProvider = Detail::TreeViewPathSelector::InfoProvider;
    using Confirmation = UserInterface::Dialog::Confirmation;

    TreeViewPathSelector(QPointer<const QAbstractItemView>      view,
                         otn::weak_optional<const InfoProvider> info_provider,
                         Confirmation confirmation = Confirmation::None)
        : m_detail{std::move(view),
                   std::move(info_provider),
                   confirmation}
    {}

private:
    PathList repositories() const override
    { return m_detail.repositories(); }

    ProviderEventDelegate& hasRepositoriesChanged() const override
    { return m_detail.hasRepositoriesChanged(); }

    Detail::TreeViewPathSelector m_detail;
};

} // namespace GitComplex::Repository::Selector::View::Concrete
