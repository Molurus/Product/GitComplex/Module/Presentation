/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Selection/Entity/Provider.h>
#include <GitComplex/Repository/Selector/View/Detail/TreeViewIdSelector.h>
#include <GitComplex/Repository/Selector/View/Concrete/Namespace.h>

/*!
 * \file include/GitComplex/Repository/Selector/View/Concrete/TreeViewIdSelector.h
 * \brief include/GitComplex/Repository/Selector/View/Concrete/TreeViewIdSelector.h
 */

namespace GitComplex::Repository::Selector::View::Concrete
{

/*!
 * \class TreeViewIdSelector
 * \brief TreeViewIdSelector.
 *
 * \headerfile GitComplex/Repository/Selector/View/Concrete/TreeViewIdSelector.h
 *
 * TreeViewIdSelector.
 *
 * \note [[Abstraction::Concrete]] [[Viper::View]] [[Framework::Qt]]
 */
class TreeViewIdSelector final : public Selection::Entity::Provider
{
public:
    using InfoProvider = Detail::TreeViewIdSelector::InfoProvider;

    TreeViewIdSelector(QPointer<const QAbstractItemView>      view,
                       otn::weak_optional<const InfoProvider> info_provider)
        : m_detail{std::move(view), std::move(info_provider)}
    {}

private:
    using Mode = Detail::TreeViewIdSelector::Mode;
    using EventDelegate = Detail::TreeViewIdSelector::EventDelegate;

    IdList repositories(Mode mode) const override
    { return m_detail.repositories(mode); }

    EventDelegate& hasRepositoriesChanged() const override
    { return m_detail.hasRepositoriesChanged(); }

    Detail::TreeViewIdSelector m_detail;
};

} // namespace GitComplex::Repository::Selector::View::Concrete
