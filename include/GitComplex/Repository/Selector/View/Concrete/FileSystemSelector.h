/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/RootList/Entity/Provider.h>
#include <GitComplex/Repository/Selector/View/Detail/FileSystemSelector.h>
#include <GitComplex/Repository/Selector/View/Concrete/Namespace.h>
#include <GitComplex/Settings/Storable.h>

/*!
 * \file include/GitComplex/Repository/Selector/View/Concrete/FileSystemSelector.h
 * \brief include/GitComplex/Repository/Selector/View/Concrete/FileSystemSelector.h
 */

namespace GitComplex::Repository::Selector::View::Concrete
{

/*!
 * \class FileSystemSelector
 * \brief FileSystemSelector.
 *
 * \headerfile GitComplex/Repository/Selector/View/Concrete/FileSystemSelector.h
 *
 * FileSystemSelector.
 *
 * \note [[Abstraction::Concrete]] [[Viper::View]] [[Framework::Qt]]
 */
class FileSystemSelector final : public RootList::Entity::Provider,
                                 public Settings::RepositoryStorable
{
public:
    FileSystemSelector() = default;

    void save(Settings::RepositoryStorage& storage) const override { m_detail.save(storage); }
    void load(Settings::RepositoryStorage& storage) override       { m_detail.load(storage); }

private:
    PathList repositories() const override
    { return m_detail.repositories(); }

    ProviderEventDelegate& hasRepositoriesChanged() const override
    { return m_detail.hasRepositoriesChanged(); }

    Detail::FileSystemSelector m_detail;
};

} // namespace GitComplex::Repository::Selector::View::Concrete
