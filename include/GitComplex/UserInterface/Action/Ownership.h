/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <QAction>
#include <QPointer>

#include <otn/all.hpp>

/*!
 * \file include/GitComplex/UserInterface/Action/Ownership.h
 * \brief include/GitComplex/UserInterface/Action/Ownership.h
 */

namespace GitComplex::UserInterface::Action
{

/*!
 * The owner of an action.
 *
 * \note [[Abstraction::Alias]] [[Viper::Presenter]] [[Framework::Qt]]
 */
using Owner = otn::slim::unique_single<QAction>;

/*!
 * Reference to an action.
 *
 * \note [[Abstraction::Alias]] [[Viper::Presenter]] [[Framework::Qt]]
 */
using Reference = QPointer<QAction>;

} // namespace GitComplex::UserInterface::Action
