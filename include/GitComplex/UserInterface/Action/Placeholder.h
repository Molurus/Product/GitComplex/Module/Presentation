/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

/*!
 * \file include/GitComplex/UserInterface/Action/Placeholder.h
 * \brief include/GitComplex/UserInterface/Action/Placeholder.h
 */

namespace GitComplex::UserInterface::Action
{

/*!
 * \class Placeholder
 * \brief Placeholder for a list of actions set.
 *
 * \headerfile GitComplex/UserInterface/Action/Placeholder.h
 *
 * Placeholder.
 *
 * \note [[Abstraction::Interface]] [[Viper::View]] [[Framework::Qt]]
 */
template <class ... ActionSetList>
class Placeholder : public Placeholder<ActionSetList>...
{
public:
    using Placeholder<ActionSetList>::emplaceActions ...;
};

/*!
 * \class Placeholder<ActionSet>
 * \brief Placeholder for one set of actions.
 *
 * \headerfile GitComplex/UserInterface/Action/Placeholder.h
 *
 * Placeholder.
 *
 * \note [[Abstraction::Interface]] [[Viper::View]] [[Framework::Qt]]
 */
template <class ActionSet>
class Placeholder<ActionSet>
{
public:
    virtual ~Placeholder() = default;

    virtual void emplaceActions(const ActionSet& actions) = 0;
};

} // namespace GitComplex::UserInterface::Action
