/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/UserInterface/Widget/Ownership.h>

#include <GitComplex/Integration/Otl/Qt.h>

#include <variant>

/*!
 * \file include/GitComplex/UserInterface/Widget/Holder.h
 * \brief include/GitComplex/UserInterface/Widget/Holder.h
 */

namespace GitComplex::UserInterface::Widget
{

/*!
 * \class Holder
 * \brief Holder.
 *
 * \headerfile GitComplex/UserInterface/Widget/Holder.h
 *
 * Holder.
 *
 * \note [[Abstraction::TemplateConcrete]] [[Viper::View]] [[Framework::Qt]]
 */
template <class WidgetType_ = QWidget>
class Holder
{
public:
    using WidgetType = WidgetType_;
    using Pointer    = QPointer<WidgetType>;

    using Owner     = Widget::Owner<WidgetType>;
    using Reference = Widget::Reference<WidgetType>;

    using WidgetPointer   = std::add_pointer_t<WidgetType>;
    using WidgetReference = std::add_lvalue_reference_t<WidgetType>;

    constexpr Holder() : m_widget{Reference{}} {}

    // Copy constructors.
    Holder(const Holder& other) = delete;

    template <class OtherWidgetType>
    Holder(const Holder<OtherWidgetType>& other) = delete;

    template <class OtherWidgetType>
    Holder(const Widget::Owner<OtherWidgetType>& widget) = delete;

    template <class OtherWidgetType>
    Holder(const Widget::Reference<OtherWidgetType>& widget) = delete;

    // Move constructors.
    template <class OtherWidgetType>
    Holder(Holder<OtherWidgetType>&& other) noexcept
        : Holder{}
    { this->swap(other); }

    template <class OtherWidgetType>
    Holder(Widget::Owner<OtherWidgetType>&& widget) noexcept
        : m_widget{std::move(widget)}
    {}

    template <class OtherWidgetType>
    Holder(Widget::Reference<OtherWidgetType>&& widget) noexcept
        : m_widget{Reference{std::move(widget)}}
    {}

    // Copy operators.
    Holder& operator=(const Holder& other) = delete;

    template <class OtherWidgetType>
    Holder& operator=(const Holder<OtherWidgetType>& other) = delete;

    template <class OtherWidgetType>
    Holder& operator=(const Widget::Owner<OtherWidgetType>& widget) = delete;

    template <class OtherWidgetType>
    Holder& operator=(const Widget::Reference<OtherWidgetType>& widget) = delete;

    // Move operators.
    template <class OtherWidgetType>
    Holder& operator=(Holder<OtherWidgetType>&& other) noexcept
    {
        Holder{std::move(other)}.swap(*this);
        return *this;
    }

    template <class OtherWidgetType>
    Holder& operator=(Widget::Owner<OtherWidgetType>&& widget) noexcept
    {
        Holder{std::move(widget)}.swap(*this);
        return *this;
    }

    template <class OtherWidgetType>
    Holder& operator=(Widget::Reference<OtherWidgetType>&& widget) noexcept
    {
        Holder{Reference{std::move(widget)}}.swap(*this);
        return *this;
    }

    ~Holder()
    {
        // TODO: Check use cases with different ownership.
        if (auto widget = std::get_if<Reference>(&m_widget))
            if (*widget)
                (**widget).setParent(nullptr);
    }

    // Conversions
    explicit operator bool() const noexcept
    {
        return std::visit(
            [](const auto& widget)
            {
                using Widget = std::decay_t<decltype(widget)>;
                if constexpr (std::is_same_v<Widget, Owner>)
                    return true;
                else
                    return static_cast<bool>(widget);
            },
            m_widget);
    }

    template <class OtherWidgetType>
    operator OtherWidgetType*() & noexcept { return get(); }

    // Observers
    WidgetPointer get() const noexcept
    {
        return std::visit(
            [](auto& widget)
            {
                using Widget = std::decay_t<decltype(widget)>;
                if constexpr (std::is_same_v<Widget, Owner>)
                    return WidgetPointer{widget};
                else
                    return widget.data();
            },
            m_widget);
    }
    WidgetPointer   operator->() const noexcept { return get(); }
    WidgetReference operator*() const { return *get(); }

    template <class OtherWidgetType>
    void swap(Holder<OtherWidgetType>& other) noexcept
    { m_widget.swap(other.m_widget); }

private:
    using Storage = std::variant<Owner, Reference>;
    Storage m_widget;
};

} // namespace GitComplex::UserInterface::Widget
