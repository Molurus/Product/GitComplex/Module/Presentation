/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Console/View/Detail/Namespace.h>
#include <GitComplex/Shell/Command/Qt/Line.h>
#include <GitComplex/Settings/Storage.h>

/*!
 * \file include/GitComplex/Console/View/Detail/History.h
 * \brief include/GitComplex/Console/View/Detail/History.h
 */

namespace GitComplex::Console::View::Detail
{

// TODO: Move the History to the Entity or Interactor.

/*!
 * \class History
 * \brief History.
 *
 * \headerfile GitComplex/Console/View/Detail/History.h
 *
 * History.
 *
 * \note [[Abstraction::Detail]] [[Viper::View]] [[Framework::Qt]]
 */
class History : public QStringList
{
public:
    History() = default;

    void appendCommand(const Command::Line& command);

    Command::Line previousCommand();
    Command::Line nextCommand();

    bool isBrowsing() const { return m_is_browsing; }
    void resetBrowse();

    void save(Settings::RepositoryStorage& storage) const;
    void load(Settings::RepositoryStorage& storage);

protected:
    static const int MaxCount = 50;

    Command::Line    startBrowse();

    void removeExcessCommands();

private:
    bool m_is_browsing{false};
    QStringList::const_reverse_iterator current_iterator;
};

} // namespace GitComplex::Console::View::Detail
