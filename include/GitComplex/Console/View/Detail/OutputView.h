/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Console/View/Detail/Namespace.h>
#include <GitComplex/Shell/Command/Qt/Summary.h>

#include <QTextEdit>

#include <GitComplex/Presentation/Export.h>

/*!
 * \file include/GitComplex/Console/View/Detail/OutputView.h
 * \brief include/GitComplex/Console/View/Detail/OutputView.h
 */

namespace GitComplex::Console::View::Detail
{

/*!
 * \class OutputView
 * \brief Console output view.
 *
 * \headerfile GitComplex/Console/View/Detail/OutputView.h
 *
 * The view output of the console.
 *
 * \note [[Abstraction::Detail]] [[Viper::View]] [[Framework::Qt]]
 */
class GITCOMPLEX_PRESENTATION_EXPORT
        OutputView : public QTextEdit
{
    Q_OBJECT

public:
    enum class AppendPlace
    {
        Top,
        Bottom
    };

    /*! Constructs a console output view with a parent.*/
    explicit OutputView(QWidget* parent = nullptr);

    void print(const Command::SummaryList& summaries);

protected:
    void print(const Command::Summary& summary);
    void printOutputLines(const Command::Output::Lines& text,
                          const QTextCharFormat&        format);
    void printError(const Command::Summary& summary);
    void printExitStatus(const Command::Summary& summary);
    void print(const QString& text, const QTextCharFormat& format);
    void printSeparator();

private:
    void setupFormats();

    QTextCharFormat m_command_format;
    QTextCharFormat m_working_directory_format;
    QTextCharFormat m_error_format;
    QTextCharFormat m_exit_status_format;
    QTextCharFormat m_output_result_format;
    QTextCharFormat m_output_error_format;
    AppendPlace     m_append_place{AppendPlace::Top};
};

} // namespace GitComplex::Console::View::Detail
