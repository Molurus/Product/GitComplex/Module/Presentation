/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Console/View/Detail/History.h>

#include <QPlainTextEdit>

#include <GitComplex/Presentation/Export.h>

/*!
 * \file include/GitComplex/Console/View/Detail/Editor.h
 * \brief include/GitComplex/Console/View/Detail/Editor.h
 */

namespace GitComplex::Console::View::Detail
{

/*!
 * \class Editor
 * \brief Console command editor.
 *
 * \headerfile GitComplex/Console/View/Detail/Editor.h
 *
 * The editor of the console command.
 *
 * \note [[Abstraction::Detail]] [[Viper::View]] [[Framework::Qt]]
 */
class GITCOMPLEX_PRESENTATION_EXPORT
        Editor : public QPlainTextEdit
{
    Q_OBJECT

public:
    /*! Constructs a console command editor with a parent.*/
    explicit Editor(QWidget* parent = nullptr);

    void save(Settings::RepositoryStorage& storage) const;
    void load(Settings::RepositoryStorage& storage);

    /*! Returns the current command. */
    Command::Line currentCommand();

protected:
    /*! Reimplemented from QPlainTextEdit::keyPressEvent(). */
    void keyPressEvent(QKeyEvent* event) override;
    /*! Reimplemented from QPlainTextEdit::mouseDoubleClickEvent(). */
    void mouseDoubleClickEvent(QMouseEvent* event) override;
    /*! Reimplemented from QPlainTextEdit::wheelEvent(). */
    void wheelEvent(QWheelEvent* event) override;

    /*! Prepares the command. */
    Command::Line prepareCommand();

    /*! Emits the commandExecutionRequested() signal. */
    void executeCommand(const Command::Line& command);

    void executeCurrentCommand();
    void appendCurrentCommandToHistory();
    void clearCurrentCommand();

    void browseHistoryPrevious();
    void browseHistoryNext();

signals:
    void commandExecutionRequested(const Command::Line& command);

private:
    History m_history;
};

} // namespace GitComplex::Console::View::Detail
