/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Console/View/Detail/Namespace.h>
#include <GitComplex/Settings/Storable.h>

#include <QSplitter>
#include <QStackedWidget>

#include <GitComplex/Presentation/Export.h>

/*!
 * \file include/GitComplex/Console/View/Detail/StackedSplitter.h
 * \brief include/GitComplex/Console/View/Detail/StackedSplitter.h
 */

namespace GitComplex::Console::View::Detail
{

/*!
 * \class StackedSplitter
 * \brief StackedSplitter.
 *
 * \headerfile GitComplex/Console/View/Detail/StackedSplitter.h
 *
 * StackedSplitter.
 *
 * \note [[using Abstraction : Detail, Concrete]]
 *       [[Viper::View]] [[Framework::Qt]]
 */
class GITCOMPLEX_PRESENTATION_EXPORT
        StackedSplitter : public QSplitter,
                          public Settings::RepositoryStorable
{
    Q_OBJECT

    // NOTE: Perhaps the interface is needed instead of the property.
    Q_PROPERTY(int currentIndex READ currentIndex WRITE setCurrentIndex)

public:
    template <std::size_t SplittedCount>
    using SplittedArray = std::array<QWidget*, SplittedCount>;

    template <std::size_t StackedCount, std::size_t SplittedCount>
    using StackedArray = std::array<SplittedArray<SplittedCount>, StackedCount>;

    template <std::size_t StackedCount, std::size_t SplittedCount>
    StackedSplitter(StackedArray<StackedCount, SplittedCount> widget_array,
                    QWidget* parent = nullptr);

    int  currentIndex() const { return m_current_index; }
    void setCurrentIndex(int index);

    void save(Settings::RepositoryStorage& storage) const override;
    void load(Settings::RepositoryStorage& storage) override;

private:
    int m_current_index{};
};

template <std::size_t StackedCount, std::size_t SplittedCount>
inline
StackedSplitter::StackedSplitter(
    StackedArray<StackedCount, SplittedCount> widget_array,
    QWidget* parent)
    : QSplitter{::Qt::Vertical, parent}
{
    static_assert(StackedCount > 0 && SplittedCount > 0);

    for (std::size_t i = 0; i < SplittedCount; ++i)
    {
        auto stack = new QStackedWidget(this);
        for (std::size_t j = 0; j < StackedCount; ++j)
            stack->addWidget(widget_array[j][i]);
        stack->setCurrentIndex(0);
        addWidget(stack);
    }
}

} // namespace GitComplex::Console::View::Detail
