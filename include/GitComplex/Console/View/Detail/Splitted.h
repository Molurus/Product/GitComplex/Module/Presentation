/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Console/View/Detail/Editor.h>
#include <GitComplex/Console/View/Detail/OutputView.h>
#include <GitComplex/Console/Entity/EventDelegate/Single.h>
#include <GitComplex/UserInterface/Widget/Ownership.h>

#include <otn/all.hpp>

#include <GitComplex/Presentation/Export.h>

/*!
 * \file include/GitComplex/Console/View/Detail/Splitted.h
 * \brief include/GitComplex/Console/View/Detail/Splitted.h
 */

namespace GitComplex::Console::View::Detail
{

/*!
 * \class Splitted
 * \brief Splitted.
 *
 * \headerfile GitComplex/Console/View/Detail/Splitted.h
 *
 * Splitted.
 *
 * \note [[Abstraction::Detail]] [[Viper::View]] [[Framework::Qt]]
 */
class GITCOMPLEX_PRESENTATION_EXPORT
        Splitted
{
public:
    using SingleEventDelegate = Entity::EventDelegate::Single;

    Splitted(Widget::Reference<Editor>     editor,
             Widget::Reference<OutputView> output);

    Command::Line currentCommand() const;
    void printCommandSummaries(const Command::SummaryList& summaries);
    void clearOutput();

    void adjustHasTarget(bool has_target);

    SingleEventDelegate& eventDelegate() const
    { return *m_event_delegate; }

    void save(Settings::RepositoryStorage& storage) const;
    void load(Settings::RepositoryStorage& storage);

private:
    Widget::Reference<Editor>     m_editor;
    Widget::Reference<OutputView> m_output;
    otn::slim::unique_single<SingleEventDelegate> m_event_delegate;
};

} // namespace GitComplex::Console::View::Detail
