/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Console/Entity/Single.h>
#include <GitComplex/Console/View/Detail/Splitted.h>
#include <GitComplex/Settings/Storable.h>

/*!
 * \file include/GitComplex/Console/View/Concrete/Splitted.h
 * \brief include/GitComplex/Console/View/Concrete/Splitted.h
 */

namespace GitComplex::Console::View::Concrete
{

/*!
 * \class Splitted
 * \brief Splitted.
 *
 * \headerfile GitComplex/Console/View/Concrete/Splitted.h
 *
 * Splitted.
 *
 * \note [[Abstraction::Concrete]] [[Viper::View]] [[Framework::Qt]]
 */
class Splitted final : public Entity::Single,
                       public Settings::RepositoryStorable
{
public:
    Splitted(Widget::Reference<Detail::Editor>     editor,
             Widget::Reference<Detail::OutputView> output)
        : m_detail{std::move(editor),
                   std::move(output)}
    {}

private:
    using SingleEventDelegate = Detail::Splitted::SingleEventDelegate;

    Command::Line currentCommand() const override
    { return m_detail.currentCommand(); }

    void printCommandSummaries(const Command::SummaryList& summaries) override
    { m_detail.printCommandSummaries(summaries); }

    void clearOutput() override
    { m_detail.clearOutput(); }

    void adjustHasTarget(bool has_target) override
    { m_detail.adjustHasTarget(has_target); }

    SingleEventDelegate& commandExecutionRequested() const override
    { return m_detail.eventDelegate(); }

    void save(Settings::RepositoryStorage& storage) const override
    { return m_detail.save(storage); }

    void load(Settings::RepositoryStorage& storage) override
    { return m_detail.load(storage); }

private:
    Detail::Splitted m_detail;
};

} // namespace GitComplex::Console::View::Concrete
