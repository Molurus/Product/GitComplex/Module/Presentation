/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/BatchWorkspace/View/ObserverPlaceholder.h>
#include <GitComplex/UserInterface/Widget/Holder.h>

#include <QMenu>
#include <QToolBar>

#include <GitComplex/Presentation/Export.h>

/*!
 * \file include/GitComplex/BatchWorkspace/View/Detail/ObserverPlaceholder.h
 * \brief include/GitComplex/BatchWorkspace/View/Detail/ObserverPlaceholder.h
 */

namespace Ui
{

class RepositoryObserverPlaceholderWidget;

}

namespace GitComplex::BatchWorkspace::View::Detail
{

/*!
 * \class ObserverPlaceholder
 * \brief ObserverPlaceholder.
 *
 * \headerfile GitComplex/BatchWorkspace/View/Detail/ObserverPlaceholder.h
 *
 * ObserverPlaceholder.
 *
 * \note [[using Abstraction : Detail, Concrete]]
 *       [[Viper::View]] [[Framework::Qt]]
 */
class GITCOMPLEX_PRESENTATION_EXPORT
        ObserverPlaceholder final : public View::ObserverPlaceholder
{
public:
    ObserverPlaceholder(QWidget* parent = nullptr, ::Qt::WindowFlags flags = {});
    ~ObserverPlaceholder() override;

private:
    void emplaceWidget(Slot::GenericObserver observer) override;

    void emplaceActions(const RootListEditorActions& actions) override;
    void emplaceActions(const SynchronizerActions::TreeStructure& actions) override;
    void emplaceActions(const SynchronizerActions::TreeStatus& actions) override;

    void setupUi();
    void setupSynchronizerToolBar();
    void setupEditorMenu();

    void showEditorMenu(const QPoint& position_in_observer);

    //
    QToolBar& synchronizerToolBar();
    QMenu&    editorMenu() { return *m_editor_menu; }

    Ui::RepositoryObserverPlaceholderWidget* ui;
    Widget::Holder<>      m_observer;
    QMenu* m_editor_menu;
    RootListEditorActions m_root_list_editor_actions;
    SynchronizerActions::TreeStructure m_tree_structure_synchronizer_actions;
    SynchronizerActions::TreeStatus    m_tree_status_synchronizer_actions;
};

} // namespace GitComplex::BatchWorkspace::View::Detail
