/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/BatchWorkspace/View/ConsolePlaceholder.h>
#include <GitComplex/Settings/Storable.h>

#include <QToolBar>

#include <GitComplex/Presentation/Export.h>

/*!
 * \file include/GitComplex/BatchWorkspace/View/Detail/ConsolePlaceholder.h
 * \brief include/GitComplex/BatchWorkspace/View/Detail/ConsolePlaceholder.h
 */

namespace Ui
{

class ConsolePlaceholderWidget;

}

namespace GitComplex::BatchWorkspace::View::Detail
{

/*!
 * \class ConsolePlaceholder
 * \brief ConsolePlaceholder.
 *
 * \headerfile GitComplex/BatchWorkspace/View/Detail/ConsolePlaceholder.h
 *
 * ConsolePlaceholder.
 *
 * \note [[using Abstraction : Detail, Concrete]]
 *       [[Viper::View]] [[Framework::Qt]]
 */
class GITCOMPLEX_PRESENTATION_EXPORT
        ConsolePlaceholder final : public View::ConsolePlaceholder,
                                   public Settings::RepositoryStorable
{
public:
    ConsolePlaceholder(QWidget* parent = nullptr, ::Qt::WindowFlags flags = {});
    ~ConsolePlaceholder() override;

private:
    using BatchConsoleActions = Presenter::Actions::BatchConsole;

    void save(Settings::RepositoryStorage& storage) const override;
    void load(Settings::RepositoryStorage& storage) override;

    void emplaceWidget(Slot::ConsoleArray console_array) override;
    void emplaceWidget(Slot::SelectionSwitcher switcher) override;
    void emplaceActions(const BatchConsoleActions& actions) override;

    void setupUi();
    void setupToolBar();

    //
    QToolBar& toolBar();

    Ui::ConsolePlaceholderWidget* ui;
    Widget::Holder<QWidget>       m_console_array;
    Widget::Holder<QWidget>       m_selection_switcher;
    BatchConsoleActions m_batch_console_actions;
};

} // namespace GitComplex::BatchWorkspace::View::Detail
